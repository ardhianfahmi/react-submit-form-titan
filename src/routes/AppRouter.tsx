import React from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Login from '../pages/LoginPage';
import Register from '../pages/RegisterPage';
import SubmitFormPage from '../pages/SubmitFormPage';
import HomePage from '../pages/HomePage';
import AfterSubmitPage from '../pages/AfterSubmitPage';

const AppRouter: React.FC = () => {
    const isTokenPresent = localStorage.token !== undefined;

    return (
        <Router>
            <Routes>
                <Route path="/" element={<Login />} />
                <Route path="/register" element={<Register />} />
                {isTokenPresent ? (
                    <>
                        <Route path="/" element={<HomePage />} />
                        <Route path="/register" element={<HomePage />} />
                        <Route path="/home" element={<HomePage />} />
                        <Route path="/submit-form" element={<SubmitFormPage />} />
                        <Route path="/after-submit" element={<AfterSubmitPage />} />
                    </>
                ) : (
                    <Route path="*" element={<Navigate to="/" />} />
                )}
            </Routes>
        </Router>
    );
};

export default AppRouter;