import Page from "../components/Page";
import HomeSections from "../sections/home-sections/HomeSections";


function HomePage() {
    return (
        <Page title='Uang Kuliah Tunggal | Home'>
            <HomeSections />
        </Page>
    );
}

export default HomePage;
