import Page from "../components/Page";
import SubmitFormSections from "../sections/submit-form-sections/SubmitFormSections";

function SubmitFormPage() {
    return (
        <Page title='Uang Kuliah Tunggal | Form'>
            <SubmitFormSections />
        </Page>
    );
}

export default SubmitFormPage;
