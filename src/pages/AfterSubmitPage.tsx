import Page from "../components/Page";
import AfterSubmitSections from "../sections/after-submit-sections/AfterSubmitSections";

function AfterSubmitPage() {
    return (
        <Page title='Uang Kuliah Tunggal | Home'>
            <AfterSubmitSections />
        </Page>
    );
}

export default AfterSubmitPage;
