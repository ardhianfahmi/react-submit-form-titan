import Page from "../components/Page";
import { RegisterSections } from "../sections/register-sections/RegisterSections";

function RegisterPage() {
    return (
        <Page title='Uang Kuliah Tunggal | Register'>
            <RegisterSections />
        </Page>
    );
}

export default RegisterPage;
