import Page from "../components/Page";
import LoginSections from "../sections/login-sections/LoginSections";


function LoginPage() {
    return (
        <Page title='Uang Kuliah Tunggal | Login'>
            <LoginSections />
        </Page>
    );
}

export default LoginPage;
