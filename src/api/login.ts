import axios from 'axios';

interface LoginResponse {
    data: {
        token: string;
        message: string;
        status: number;
    }
    // data: Object;
}

const baseURL = process.env.REACT_APP_BASE_URL || 'http://103.171.85.14:1323'; // Default URL

const instance = axios.create({
    baseURL,
});

export const login = async (username: string, password: string): Promise<LoginResponse> => {
    try {
        const response = await instance.post<LoginResponse>('user/auth', { // Adjusted the endpoint path to remove leading '/'
            username,
            password,
        },
        {
            headers: {
                'Content-Type': 'application/json',
            },
        });

        return response.data;
    } catch (error) {
        throw error;
    }
};
