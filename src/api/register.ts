import axios from 'axios';

interface RegisterResponse {
    message: string;
}

interface RegisterData {
    fullname: string;
    username: string;
    password: string;
}

const baseURL = process.env.REACT_APP_BASE_URL || 'http://103.171.85.14:1323'; // Default URL

const instance = axios.create({
    baseURL,
});

export const register = async (data: RegisterData): Promise<RegisterResponse> => {
    try {
        const response = await instance.post<RegisterResponse>('users', data);

        return response.data;
    } catch (error) {
        throw error;
    }
};
