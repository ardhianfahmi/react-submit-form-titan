// api.ts
import axios from 'axios';

interface SubmitFormResponse {
    message: string;
    status: number;
}

interface SubmitFormData {
    fullname: string,
    phone_number: string,
    email: string,
    alamat_ktp: string,
    alamat_domisili: string,
    no_hp_ortu: string,
    no_hp_saudara: string,
    no_hp_teman: string,
    instagram: string,
    penghasilan: string,
    university: string,
    fakultas: string,
    jurusan: string,
    semester: string,
    program_studi: string,
    ktp: File,
    ktm: File,
    nim: string,
}

export const submitForm = async (data: SubmitFormData): Promise<SubmitFormResponse> => {
    try {
        const formData = new FormData();
        Object.entries(data).forEach(([key, value]) => {
            if (key === 'ktpScan' || key === 'ktmScan') {
                if (value) {
                    formData.append(key, value);
                }
            } else {
                formData.append(key, value);
            }
        });

        // eslint-disable-next-line
        const response = await axios.post<SubmitFormResponse>(process.env.REACT_APP_BASE_URL+'/form/create' || 'http://103.171.85.14:1323'+'/form/create', formData, {
            headers: {
                'Authorization': 'bearer ' + localStorage.getItem('token'), 
                'Content-Type': 'multipart/form-data',
            },
        });
        return response.data;
        
    } catch (error) {
        throw error;
    }
};
