// Navbar.tsx
import React, { useState } from 'react';
import { AppBar, Button, Toolbar, IconButton, Drawer, List, ListItem, ListItemText, Popover, Typography, Box, Container } from '@mui/material';
import { CustomAvatar } from '../components/custom-avatar';
import _mock from '../_mock';
import { useNavigate } from 'react-router-dom';

const Navbar: React.FC = () => {
    const navigate = useNavigate();

    const [isDrawerOpen, setDrawerOpen] = useState(false);

    //eslint-disable-next-line
    const handleDrawerOpen = () => {
        setDrawerOpen(true);
    };

    const handleDrawerClose = () => {
        setDrawerOpen(false);
    };

    const handleLogout = () => {
        localStorage.removeItem('token');
        navigate('/');
    };

    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;

    return (
        <div>
            <AppBar position="static" sx={{
                background: '#DDDEDF',
                display: 'flex',
                flexDirection: 'row',
                position: 'absolute',
                boxShadow: 'none !important',
            }}>
                <Container>
                    <Toolbar sx={{ justifyContent: 'space-between' }}>
                        <Box>
                            <img src='logo/nav-logo.png' alt='' width={'36px'} />
                        </Box>
                        <IconButton edge="start" color="inherit" aria-label="menu" onClick={handleClick}>
                            <CustomAvatar color='primary' src={_mock.image.avatar(1)}></CustomAvatar>
                        </IconButton>
                    </Toolbar>
                </Container>
            </AppBar>

            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
            >
                <Button onClick={handleLogout}>
                    <Typography sx={{ p: 2 }}>Logout</Typography>
                </Button>
            </Popover>

            <Drawer anchor="left" open={isDrawerOpen} onClose={handleDrawerClose} sx={{ background: 'none !important' }}>
                <List>
                    <ListItem button onClick={handleDrawerClose}>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
            </Drawer>
        </div>
    );
};

export default Navbar;
