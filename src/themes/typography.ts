import '@fontsource/nunito';

// ----------------------------------------------------------------------

export function remToPx(value: string) {
  return Math.round(parseFloat(value) * 16);
}

export function pxToRem(value: number) {
  return `${value / 16}rem`;
}

export function responsiveFontSizes({ sm, md, lg }: { sm: number; md: number; lg: number }) {
  return {
    '@media (min-width:600px)': {
      fontSize: pxToRem(sm),
    },
    '@media (min-width:900px)': {
      fontSize: pxToRem(md),
    },
    '@media (min-width:1200px)': {
      fontSize: pxToRem(lg),
    },
  };
}

// ----------------------------------------------------------------------

const FONT_PRIMARY = 'Nunito, sans-serif';
const FONT_PRIMARY_AR = 'Nunito, sans-serif';

const typography = (lang: string) =>
  ({
    fontFamily: lang === 'en' ? FONT_PRIMARY : FONT_PRIMARY_AR,
    fontWeightRegular: 400,
    fontWeightMedium: 600,
    fontWeightBold: 700,
    h1: {
      fontWeight: 700,
      lineHeight: pxToRem(40),
      fontSize: pxToRem(32),
      ...responsiveFontSizes({ sm: 52, md: 58, lg: 64 }),
    },
    h2: {
      fontWeight: 700,
      lineHeight: pxToRem(37),
      fontSize: pxToRem(24),
      ...responsiveFontSizes({ sm: 40, md: 44, lg: 48 }),
    },
    h3: {
      fontWeight: 600,
      lineHeight: pxToRem(37),
      fontSize: pxToRem(20),
      ...responsiveFontSizes({ sm: 26, md: 30, lg: 32 }),
    },
    h4: {
      fontWeight: 600,
      lineHeight: pxToRem(37),
      fontSize: pxToRem(18),
      ...responsiveFontSizes({ sm: 20, md: 24, lg: 24 }),
    },
    h5: {
      fontWeight: 500,
      lineHeight: 1.1,
      fontSize: pxToRem(12),
      ...responsiveFontSizes({ sm: 19, md: 20, lg: 20 }),
    },
    h6: {
      fontWeight: 400,
      lineHeight: 28 / 18,
      fontSize: pxToRem(12),
      ...responsiveFontSizes({ sm: 18, md: 18, lg: 18 }),
    },
    subtitle1: {
      fontWeight: 500,
      lineHeight: pxToRem(37),
      fontSize: pxToRem(18),
    },
    subtitle2: {
      fontWeight: 400,
      lineHeight: pxToRem(37),
      fontSize: pxToRem(16),
    },
    body1: {
      lineHeight: pxToRem(37),
      fontSize: pxToRem(16),
    },
    body2: {
      lineHeight: pxToRem(16),
      fontSize: pxToRem(14),
    },
    caption: {
      lineHeight: pxToRem(16),
      fontSize: pxToRem(14),
    },
    overline: {
      fontWeight: 700,
      lineHeight: 1.5,
      fontSize: pxToRem(12),
      textTransform: 'uppercase',
    },
    button: {
      fontWeight: 600,
      lineHeight: pxToRem(24),
      fontSize: pxToRem(16),
      textTransform: 'capitalize',
    },
  } as const);

export default typography;
