import { HelmetProvider } from 'react-helmet-async';
import { forwardRef, ReactNode } from 'react';
import type { BoxProps } from '@mui/material';
import { Box } from '@mui/material';
import { Helmet } from 'react-helmet-async';

interface Props extends BoxProps {
    children: ReactNode;
    meta?: ReactNode;
    title: string;
}

const Page = forwardRef<HTMLDivElement, Props>(({ children, title = '', meta, ...other }, ref) => (
    <Box ref={ref} className="main" sx={{ minHeight: 'calc(100vh - 110px)', height: '100%' }} {...other}>
        {/* Wrap Helmet component in HelmetProvider */}
        <HelmetProvider>
            <Helmet>
                <title>{title}</title>
                {meta}
            </Helmet>
        </HelmetProvider>
        {children}
    </Box>
));

export default Page;
