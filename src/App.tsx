// App.tsx
import React from 'react';
import AppRouter from './routes/AppRouter';
import ThemeProvider from './themes/index';

const App: React.FC = () => {
  return (
    <ThemeProvider>
      <div className="App">
        <AppRouter />
      </div>
    </ThemeProvider>
  );
};

export default App;
