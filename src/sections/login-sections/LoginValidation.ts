import * as yup from 'yup';

const LoginValidation = yup.object({
  username: yup.string().required('Nama Pengguna wajib diisi'),
  password: yup.string().required('Password wajib diisi').min(8, 'Password minimal 8 karakter'),
});

export default LoginValidation;
