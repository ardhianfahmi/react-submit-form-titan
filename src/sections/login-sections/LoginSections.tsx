import { FC, useEffect, useState } from 'react';
import { Alert, Button, Card, CardContent, Container, Box, Grid, Typography, Link, TextField, Snackbar, IconButton, InputAdornment } from '@mui/material';
import { useFormik } from 'formik';
import validationSchema from './LoginValidation';
import { login } from '../../api/login'; // Import the new module
import { useNavigate } from 'react-router-dom';
import Iconify from '../../components/iconify';

interface FormValues {
    username: string;
    password: string;
}

const LoginSections: FC = () => {

    const navigate = useNavigate();
    const [sessionTimeout, setSessionTimeout] = useState<NodeJS.Timeout | null>(null);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [showPassword, setShowPassword] = useState(false);

    const handlePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };
    const formik = useFormik<FormValues>({
        initialValues: {
            username: '',
            password: '',
        },
        validationSchema,
        onSubmit: async (values) => {
            try {
                const response = await login(values.username, values.password);

                if (response.data.status === 200) {
                    localStorage.setItem('token', response.data.token);

                    const timeoutId = setTimeout(() => {
                        localStorage.removeItem('token');
                        navigate('/login');
                    }, 60 * 60 * 1000);

                    setSnackbarMessage('Login Berhasil !');
                    setSnackbarOpen(true);

                    setSessionTimeout(timeoutId);

                    if (values.username && values.password) {
                        navigate('/home');
                    }
                } else {
                    localStorage.setItem('token', response.data.token);

                    const timeoutId = setTimeout(() => {
                        localStorage.removeItem('token');
                        navigate('/login');
                    }, 60 * 60 * 1000);

                    setSnackbarMessage('Login Success!');
                    setSnackbarOpen(true);

                    setSessionTimeout(timeoutId);

                    if (values.username && values.password) {
                        navigate('/home');
                    }
                }
            } catch (error) {
                if (error instanceof Error) {
                    setSnackbarMessage('Pastikan Username & Password anda benar');
                } else {
                    setSnackbarMessage(`${error}` != null ? 'Pastikan Username & Password anda benar' : `${error}`);
                }
                setSnackbarOpen(true);
            }
        },
    });

    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    };

    useEffect(() => {
        return () => {
            if (sessionTimeout) {
                clearTimeout(sessionTimeout);
            }
        };
    }, [sessionTimeout]);

    return (
        <Box
            sx={{
                height: '100vh',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Container>
                <Grid container spacing={2} justifyContent="center">
                    <Grid item xs={12} md={6}>
                        <Card sx={{ boxShadow: 'none' }}>
                            <CardContent sx={{ padding: { lg: '60px !important ' } }}>
                                <Typography variant="h3">Login</Typography>
                                <Typography variant="body1">
                                    Belum punya akun? <Link href="/register">Register</Link>
                                </Typography>
                                <Box
                                    component="form"
                                    onSubmit={formik.handleSubmit}
                                    sx={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        gap: { md: '10px', lg: '15px' },
                                        marginTop: { lg: '10px' },
                                    }}
                                >
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        id="username"
                                        label="Username"
                                        name="username"
                                        autoFocus
                                        value={formik.values.username}
                                        onChange={formik.handleChange}
                                        error={formik.touched.username && Boolean(formik.errors.username)}
                                        helperText={formik.touched.username && formik.errors.username}
                                    />
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type={showPassword ? 'text' : 'password'} // Toggle password visibility based on showPassword state
                                        id="password"
                                        value={formik.values.password}
                                        onChange={formik.handleChange}
                                        error={formik.touched.password && Boolean(formik.errors.password)}
                                        helperText={formik.touched.password && formik.errors.password}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        aria-label="toggle password visibility"
                                                        onClick={handlePasswordVisibility}
                                                        edge="end"
                                                    >
                                                        {showPassword ? <Iconify icon={'solar:eye-bold'} /> : <Iconify icon={'ion:eye-off'} />}
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                    />
                                    <Snackbar
                                        open={snackbarOpen}
                                        autoHideDuration={10000}
                                        onClose={handleSnackbarClose}
                                        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                                    >
                                        <Alert onClose={handleSnackbarClose} severity={'info'} sx={{ width: '100%', alignSelf: 'center' }}>
                                            {snackbarMessage}
                                        </Alert>
                                    </Snackbar>
                                    <Button type="submit" fullWidth variant="contained" sx={{ mt: 3, mb: 2 }}>
                                        Log In
                                    </Button>
                                    <Box>
                                        <Typography variant='body1' sx={{ textAlign: 'center' }}>
                                            uangkuliahtunggal.id
                                        </Typography>
                                        <Typography variant='body1' sx={{ textAlign: 'center' }}>
                                            By KSP. INDO DANA NUSANTARA
                                        </Typography>
                                    </Box>
                                </Box>
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    );
};

export default LoginSections;
