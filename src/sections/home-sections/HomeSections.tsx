import { FC } from 'react';
import { Button, Card, Container, Box, Grid, Typography, CardContent, Link } from '@mui/material';
import Navbar from '../../layouts/Navbar';
import Iconify from '../../components/iconify';

const HomeSections: FC = () => {
    return (
        <>
            <Navbar />
            <Box
                sx={{
                    height: '100vh',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Container>
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item xs={12} md={6}>
                            <Card sx={{ boxShadow: 'none' }}>
                                <CardContent sx={{ padding: { lg: '60px !important ' } }}>
                                    <Box
                                        component="form"
                                        sx={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            gap: { md: '15px', lg: '30px' },
                                            marginTop: { lg: '10px' },
                                        }}
                                    >
                                        <Typography variant="h3" sx={{ textAlign: 'center' }}>
                                            Ajukan Pembiayaan UKT
                                        </Typography>

                                        <Link href="/submit-form" style={{ textDecoration: 'none', textAlign: 'center' }}>
                                            <Button variant='contained' >
                                                <Iconify icon={'iconoir:submit-document'} sx={{ marginRight: '10px' }}></Iconify>
                                                Ajukan
                                            </Button>
                                        </Link>
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </>
    );
};

export default HomeSections;
