// RegisterValidation.ts
import * as Yup from 'yup';

const SubmitFormValidation = Yup.object().shape({
    fullname: Yup.string().required('Nama lengkap wajib diisi'),
    phone_number: Yup.string().required('Nomor WA wajib diisi'),
    email: Yup.string().email('Alamat email tidak valid').required('Alamat email wajib diisi'),
    alamat_ktp: Yup.string().required('Alamat sesuai KTP wajib diisi'),
    alamat_domisili: Yup.string().required('Alamat domisili wajib diisi'),
    no_hp_ortu: Yup.string().required('No telp orang tua wajib diisi'),
    no_hp_saudara: Yup.string().required('No telp teman terdekat wajib diisi'),
    no_hp_teman: Yup.string().required('No telp saudara wajib diisi'),
    instagram: Yup.string().required('Nama IG wajib diisi'),
    penghasilan: Yup.string().required('Penghasilan / Uang saku per bulan wajib diisi'),
    university: Yup.string().required('Universitas wajib diisi'),
    fakultas: Yup.string().required('Fakultas wajib diisi'),
    jurusan: Yup.string().required('Jurusan wajib diisi'),
    semester: Yup.string().required('Semester wajib diisi'),
    program_studi: Yup.string().required('Program studi wajib diisi'),
    ktp: Yup.mixed().required('Scan KTP wajib diisi'),
    ktm: Yup.mixed().required('Scan KTM wajib diisi'),
    nim: Yup.mixed().required('NIM/billing pembayaran UKT wajib diisi'),
});

export default SubmitFormValidation;
