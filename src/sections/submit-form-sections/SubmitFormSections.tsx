import { FC, } from 'react';
import { Card, CardContent, Container, Box, Grid, Typography } from '@mui/material';
import Navbar from '../../layouts/Navbar';
import SubmitForm from './SubmitForm';

const SubmitFormSection: FC = () => {

    return (
        <>
            <Navbar />
            <Box
                sx={{
                    height: '100vh',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Container>
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item xs={12} md={10}>
                            <Card sx={{ boxShadow: 'none', position: 'absolute', top: '10%' }}>
                                <CardContent sx={{ padding: { lg: '60px !important ' } }}>
                                    <Typography variant="h3">Registration Form</Typography>
                                    <SubmitForm />
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </>
    );
};

export default SubmitFormSection;
