import { FC, ChangeEvent, useState } from 'react';
import {
    Button, Typography, Box, TextField, Input, InputLabel, InputAdornment, Snackbar, Alert, Modal, CircularProgress
} from '@mui/material';
import { useFormik } from 'formik';
import SubmitFormValidation from './SubmitFormValidation';
import { useNavigate } from 'react-router-dom';
import { submitForm } from '../../api/submitForm';

interface SubmitFormValues {
    fullname: string;
    phone_number: string;
    email: string;
    alamat_ktp: string;
    alamat_domisili: string;
    no_hp_ortu: string;
    no_hp_saudara: string;
    no_hp_teman: string;
    instagram: string;
    penghasilan: string;
    university: string;
    fakultas: string;
    jurusan: string;
    semester: string;
    program_studi: string;
    ktp: File | null;
    ktm: File | null;
    nim: string;
}

const SubmitForm: FC = () => {

    const navigate = useNavigate();
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    };

    const handleCloseError = () => {
        setError(false);
    };

    const formik = useFormik<SubmitFormValues>({
        initialValues: {
            fullname: '',
            phone_number: '',
            email: '',
            alamat_ktp: '',
            alamat_domisili: '',
            no_hp_ortu: '',
            no_hp_saudara: '',
            no_hp_teman: '',
            instagram: '',
            penghasilan: '',
            university: '',
            fakultas: '',
            jurusan: '',
            semester: '',
            program_studi: '',
            ktp: null as File | null,
            ktm: null as File | null,
            nim: '',
        },
        validationSchema: SubmitFormValidation,
        onSubmit: async (values) => {
            setLoading(true);
            try {
                const ktpFile = values.ktp;
                const ktmFile = values.ktm;
                // consol
                if (ktpFile && ktmFile) {
                    const response = await submitForm({
                        fullname: values.fullname,
                        phone_number: values.phone_number,
                        email: values.email,
                        alamat_ktp: values.alamat_ktp,
                        alamat_domisili: values.alamat_domisili,
                        no_hp_ortu: values.no_hp_ortu,
                        no_hp_saudara: values.no_hp_saudara,
                        no_hp_teman: values.no_hp_teman,
                        instagram: values.instagram,
                        penghasilan: values.penghasilan,
                        university: values.university,
                        fakultas: values.fakultas,
                        jurusan: values.jurusan,
                        semester: values.semester,
                        program_studi: values.program_studi,
                        ktp: ktpFile,
                        ktm: ktmFile,
                        nim: values.nim,
                    });
                    if (response.status === 200 || response.status === 201) {
                        setSnackbarMessage(response.message);
                        setSnackbarOpen(true);

                        navigate('/after-submit');
                    } else {
                        setLoading(false);
                        setError(true);
                    }
                } else {
                    setSnackbarMessage('Foto KTP & KTM Tidak Boleh Kosong');
                    setSnackbarOpen(true);
                    setError(true);
                }
            } catch (error) {
                setError(true);
            } finally {
                setLoading(false); // Set loading to false regardless of success or failure
            }
        },
    });

    const handleKtpChange = (fieldName: keyof typeof formik.values) => (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            formik.setFieldValue(fieldName, file);
        }
    };

    const handleKtmChange = (fieldName: keyof typeof formik.values) => (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (file) {
            formik.setFieldValue(fieldName, file);
        }
    };

    return (
        <form onSubmit={formik.handleSubmit}>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: { md: '10px', lg: '15px' },
                    marginTop: { lg: '10px' },
                }}
            >
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="fullname"
                        label="Nama Lengkap"
                        name="fullname"
                        autoComplete="off"
                        autoFocus
                        value={formik.values.fullname}
                        onChange={formik.handleChange}
                        error={formik.touched.fullname && Boolean(formik.errors.fullname)}
                        helperText={formik.touched.fullname && formik.errors.fullname}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="phone_number"
                        label="Nomor WA"
                        name="phone_number"
                        autoComplete="off"
                        value={formik.values.phone_number}
                        onChange={formik.handleChange}
                        error={formik.touched.phone_number && Boolean(formik.errors.phone_number)}
                        helperText={formik.touched.phone_number && formik.errors.phone_number}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Alamat E-mail"
                        name="email"
                        autoComplete="off"
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        error={formik.touched.email && Boolean(formik.errors.email)}
                        helperText={formik.touched.email && formik.errors.email}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="alamat_ktp"
                        label="Alamat Sesuai KTP"
                        name="alamat_ktp"
                        autoComplete="off"
                        value={formik.values.alamat_ktp}
                        onChange={formik.handleChange}
                        error={formik.touched.alamat_ktp && Boolean(formik.errors.alamat_ktp)}
                        helperText={formik.touched.alamat_ktp && formik.errors.alamat_ktp}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="alamat_domisili"
                        label="Alamat Saat Ini"
                        name="alamat_domisili"
                        autoComplete="off"
                        value={formik.values.alamat_domisili}
                        onChange={formik.handleChange}
                        error={formik.touched.alamat_domisili && Boolean(formik.errors.alamat_domisili)}
                        helperText={formik.touched.alamat_domisili && formik.errors.alamat_domisili}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="no_hp_ortu"
                        label="Nomor Telepon Orang Tua"
                        name="no_hp_ortu"
                        autoComplete="off"
                        value={formik.values.no_hp_ortu}
                        onChange={formik.handleChange}
                        error={formik.touched.no_hp_ortu && Boolean(formik.errors.no_hp_ortu)}
                        helperText={formik.touched.no_hp_ortu && formik.errors.no_hp_ortu}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="no_hp_saudara"
                        label="Nomor Telepon Saudara Terdekat"
                        name="no_hp_saudara"
                        autoComplete="off"
                        value={formik.values.no_hp_saudara}
                        onChange={formik.handleChange}
                        error={formik.touched.no_hp_saudara && Boolean(formik.errors.no_hp_saudara)}
                        helperText={formik.touched.no_hp_saudara && formik.errors.no_hp_saudara}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="no_hp_teman"
                        label="Nomor Telepon Teman Terdekat"
                        name="no_hp_teman"
                        autoComplete="off"
                        value={formik.values.no_hp_teman}
                        onChange={formik.handleChange}
                        error={formik.touched.no_hp_teman && Boolean(formik.errors.no_hp_teman)}
                        helperText={formik.touched.no_hp_teman && formik.errors.no_hp_teman}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="university"
                        label="Universitas"
                        name="university"
                        autoComplete="off"
                        value={formik.values.university}
                        onChange={formik.handleChange}
                        error={formik.touched.university && Boolean(formik.errors.university)}
                        helperText={formik.touched.university && formik.errors.university}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="fakultas"
                        label="Fakultas"
                        name="fakultas"
                        autoComplete="off"
                        value={formik.values.fakultas}
                        onChange={formik.handleChange}
                        error={formik.touched.fakultas && Boolean(formik.errors.fakultas)}
                        helperText={formik.touched.fakultas && formik.errors.fakultas}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="jurusan"
                        label="Jurusan"
                        name="jurusan"
                        autoComplete="off"
                        value={formik.values.jurusan}
                        onChange={formik.handleChange}
                        error={formik.touched.jurusan && Boolean(formik.errors.jurusan)}
                        helperText={formik.touched.jurusan && formik.errors.jurusan}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="semester"
                        label="Semester"
                        name="semester"
                        autoComplete="off"
                        value={formik.values.semester}
                        onChange={formik.handleChange}
                        error={formik.touched.semester && Boolean(formik.errors.semester)}
                        helperText={formik.touched.semester && formik.errors.semester}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="program_studi"
                        label="Program Study"
                        name="program_studi"
                        autoComplete="off"
                        value={formik.values.program_studi}
                        onChange={formik.handleChange}
                        error={formik.touched.program_studi && Boolean(formik.errors.program_studi)}
                        helperText={formik.touched.program_studi && formik.errors.program_studi}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '10px' }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="instagram"
                        label="Instagram Pribadi"
                        name="instagram"
                        autoComplete="off"
                        value={formik.values.instagram}
                        onChange={formik.handleChange}
                        error={formik.touched.instagram && Boolean(formik.errors.instagram)}
                        helperText={formik.touched.instagram && formik.errors.instagram}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="penghasilan"
                        label="Penghasilan / Uang Saku"
                        name="penghasilan"
                        autoComplete="off"
                        value={formik.values.penghasilan}
                        onChange={formik.handleChange}
                        error={formik.touched.penghasilan && Boolean(formik.errors.penghasilan)}
                        helperText={formik.touched.penghasilan && formik.errors.penghasilan}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="nim"
                        label="NIM - Nomor Billing"
                        name="nim"
                        autoComplete="off"
                        value={formik.values.nim}
                        onChange={formik.handleChange}
                        error={formik.touched.nim && Boolean(formik.errors.nim)}
                        helperText={formik.touched.nim && formik.errors.nim}
                    />
                </Box>
                <Box sx={{ display: 'flex', flexDirection: { xs: 'column', md: 'row' }, gap: '25px' }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                        <InputLabel htmlFor="ktp" sx={{ marginTop: '10px' }}>Scan KTP</InputLabel>
                        <Input
                            type="file"
                            id="ktp"
                            name="ktp"
                            required
                            onChange={handleKtpChange('ktp')}
                            endAdornment={<InputAdornment position="end">File Upload</InputAdornment>}
                            inputProps={{ accept: 'image/*,.pdf' }}
                        />
                    </Box>
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                        <InputLabel htmlFor="ktm" sx={{ marginTop: '10px' }}>Scan KTM</InputLabel>
                        <Input
                            type="file"
                            id="ktm"
                            name="ktm"
                            required
                            onChange={handleKtmChange('ktm')}
                            endAdornment={<InputAdornment position="end">File Upload</InputAdornment>}
                            inputProps={{ accept: 'image/*,.pdf' }}
                        />
                    </Box>
                </Box>
                <Snackbar
                    open={snackbarOpen}
                    autoHideDuration={6000}
                    onClose={handleSnackbarClose}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                >
                    <Alert onClose={handleSnackbarClose} severity={'info'} sx={{ width: '100%', alignSelf: 'center' }}>
                        {snackbarMessage}
                    </Alert>
                </Snackbar>
                <Button type="submit" fullWidth variant="contained" sx={{ marginTop: '20px' }}>
                    Submit
                </Button>
            </Box>
            <Modal
                open={loading}
                aria-labelledby="loading-modal"
                aria-describedby="loading-modal-description"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <Box
                    sx={{
                        backgroundColor: 'white',
                        borderRadius: '8px',
                        padding: '20px',
                        textAlign: 'center',
                    }}
                >
                    <CircularProgress color="primary" />
                    <Typography variant="body1" sx={{ marginTop: '10px' }}>
                        Mohon tunggu. Form anda sedang diunggah.
                    </Typography>
                </Box>
            </Modal>
            <Modal
                open={error}
                aria-labelledby="loading-modal"
                aria-describedby="loading-modal-description"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <Box
                    sx={{
                        backgroundColor: 'white',
                        borderRadius: '8px',
                        padding: '20px',
                        textAlign: 'center',
                    }}
                >
                    <Typography variant="h5" sx={{ marginTop: '10px' }}>
                        Anda sudah mengunggah form.
                    </Typography>
                    <Typography variant="h5" sx={{ marginTop: '10px', marginBottom: '30px' }}>
                        Silahkan tunggu verifikasi dari admin.
                    </Typography>
                    <Button onClick={handleCloseError} fullWidth variant="contained">
                        Tutup
                    </Button>
                </Box>
            </Modal>
        </form>
    );
};

export default SubmitForm;
