import { FC, useState } from 'react';
import { Alert, Snackbar, Button, Box, TextField, IconButton, InputAdornment } from '@mui/material';
import { useFormik } from 'formik';
import RegisterValidation from './RegisterValidation';
import { register } from '../../api/register';
import { useNavigate } from 'react-router-dom';
import Iconify from '../../components/iconify';

interface RegisterFormValues {
    fullName: string;
    username: string;
    password: string;
    confirmPassword: string;
}

export const RegisterForm: FC = () => {
    const navigate = useNavigate();
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');
    const handleSnackbarClose = () => {
        setSnackbarOpen(false);
    };

    const [showPassword, setShowPassword] = useState(false);

    const handlePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const [showConfirmPassword, setShowConfirmPassword] = useState(false);

    const handleConfirmPasswordVisibility = () => {
        setShowConfirmPassword(!showConfirmPassword);
    };

    const formik = useFormik<RegisterFormValues>({
        initialValues: {
            fullName: '',
            username: '',
            password: '',
            confirmPassword: '',
        },
        validationSchema: RegisterValidation,
        onSubmit: async (values) => {
            try {
                //eslint-disable-next-line
                const response = await register({
                    fullname: values.fullName,
                    username: values.username,
                    password: values.password,
                });
                setSnackbarMessage('Registrasi Berhasil. Silahkan Login Kembali');
                setSnackbarOpen(true);

                setTimeout(() => {
                    navigate('/');
                }, 3000);
            } catch (error) {
                if (error instanceof Error) {
                    setSnackbarMessage('Username sudah digunakan. Silahkan coba yang lain.');
                } else {
                    setSnackbarMessage(`${error}` != null ? 'Username sudah digunakan. Silahkan coba yang lain.' : `${error}`);
                }
                setSnackbarOpen(true);
            }
        },
    });


    return (
        <form onSubmit={formik.handleSubmit}>
            <Box sx={{
                display: 'flex',
                flexDirection: 'column',
                gap: { xs: '10px', md: '15px' },
                marginTop: { lg: '10px' },
            }}>
                <TextField
                    id="fullName"
                    color="secondary"
                    type="text"
                    label="Nama Lengkap"
                    variant="outlined"
                    fullWidth
                    value={formik.values.fullName}
                    onChange={formik.handleChange}
                    error={formik.touched.fullName && Boolean(formik.errors.fullName)}
                    helperText={formik.touched.fullName && formik.errors.fullName}
                />
                <TextField
                    id="username"
                    color="secondary"
                    type="text"
                    label="Nama Pengguna"
                    variant="outlined"
                    fullWidth
                    value={formik.values.username}
                    onChange={formik.handleChange}
                    error={formik.touched.username && Boolean(formik.errors.username)}
                    helperText={formik.touched.username && formik.errors.username}
                />
                <TextField
                    id="password"
                    type={showPassword ? 'text' : 'password'}
                    label="Kata Sandi"
                    variant="outlined"
                    fullWidth
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={handlePasswordVisibility} edge="end">
                                    {showPassword ? <Iconify icon={'solar:eye-bold'} /> : <Iconify icon={'ion:eye-off'} />}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <TextField
                    id="confirmPassword"
                    type={showConfirmPassword ? 'text' : 'password'} // Adjusted the type attribute
                    label="Konfirmasi Kata Sandi"
                    variant="outlined"
                    fullWidth
                    value={formik.values.confirmPassword}
                    onChange={formik.handleChange}
                    error={formik.touched.confirmPassword && Boolean(formik.errors.confirmPassword)}
                    helperText={formik.touched.confirmPassword && formik.errors.confirmPassword}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={handleConfirmPasswordVisibility} edge="end">
                                    {showConfirmPassword ? <Iconify icon={'solar:eye-bold'} /> : <Iconify icon={'ion:eye-off'} />}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <Snackbar
                    open={snackbarOpen}
                    autoHideDuration={10000}
                    onClose={handleSnackbarClose}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                >
                    <Alert onClose={handleSnackbarClose} severity="info" sx={{ width: '100%', alignSelf: 'center' }}>
                        {snackbarMessage}
                    </Alert>
                </Snackbar>
                <Button type="submit" variant="contained">
                    Register
                </Button>
            </Box>
        </form>
    );
};
