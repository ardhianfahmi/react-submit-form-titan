// RegisterValidation.ts
import * as yup from 'yup';

const RegisterValidation = yup.object().shape({
  fullName: yup.string().required('Nama lengkap wajib diisi'),
  username: yup.string().required('Nama pengguna wajib diisi'),
  password: yup
    .string()
    .required('Kata sandi wajib diisi')
    .min(8, 'Kata sandi harus terdiri dari minimal 8 karakter'),
  confirmPassword: yup
    .string()
    .required('Konfirmasi kata sandi wajib diisi')
    .oneOf([yup.ref('password')], 'Konfirmasi kata sandi harus sama dengan kata sandi'),
});

export default RegisterValidation;
