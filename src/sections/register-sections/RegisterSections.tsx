import { FC } from 'react';
import {
    CardContent,
    Container,
    Box,
    Card,
    Grid,
    Typography,
    Link,
} from '@mui/material';
import { RegisterForm } from './RegisterForm';

export const RegisterSections: FC = () => {
    return (
        <Box
            sx={{
                height: '100vh',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <Container>
                <Grid container spacing={2} sx={{ justifyContent: 'center' }}>
                    <Grid item xs={12} md={6}>
                        <Card sx={{ boxShadow: 'none' }}>
                            <CardContent sx={{ padding: { lg: '60px !important ' } }}>
                                <Typography variant='h3'>
                                    Register
                                </Typography>
                                <Typography variant='body1'>
                                    Sudah punya akun?  <Link href="/">Login</Link>
                                </Typography>
                                <RegisterForm />
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    );
}