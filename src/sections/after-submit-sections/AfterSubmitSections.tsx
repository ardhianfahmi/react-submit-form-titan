import { FC } from 'react';
import { Card, Container, Box, Grid, Typography, CardContent } from '@mui/material';
import Navbar from '../../layouts/Navbar';

const AfterSubmitSections: FC = () => {
    return (
        <>
            <Navbar />
            <Box
                sx={{
                    height: '100vh',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
            >
                <Container>
                    <Grid container spacing={2} justifyContent="center">
                        <Grid item xs={12} md={6}>
                            <Card sx={{ boxShadow: 'none' }}>
                                <CardContent sx={{ padding: { lg: '60px !important ' } }}>

                                    <Typography variant="h4" sx={{ textAlign: 'center' }}>
                                        Form pengajuan anda sedang divalidasi. Mohon tunggu 1x24 jam. Ketika proses validasi selesai, kami akan memberitahu anda melalui email.
                                    </Typography>
                                    <Typography variant="h4" sx={{ textAlign: 'center' }}>
                                        Terimakasih
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </>
    );
};

export default AfterSubmitSections;
